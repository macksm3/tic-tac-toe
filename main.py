from tic_tac_toe import *


def run_game():
    """Create the game's board and run its main loop."""
    game = TicTacToeGame()
    board = TicTacToeBoard(game)
    board.mainloop()


def main():
    run_game()


if __name__ == '__main__':
    main()
